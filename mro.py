""" Explanation of Python's MRO - Method Resolution Order
"""


def proper():
    # Layer 1 - the lowest class in composition
    class A: pass

    # Layer 2
    class B(A): pass
    class C(A): pass

    # Layer 3
    class D(B, C): pass
    class E(B, C): pass

    # Layer 4
    class F(B, C): pass
    class G(D, E): pass

    # Layer 5 - the highest layer
    class I(F, G): pass

    print(I.mro())


def wrong():
    class X(object): pass
    class Y(object): pass

    class A(X, Y): pass
    class B(Y, X): pass

    # This should raise TypeError
    class C(A, B): pass


if __name__ == '__main__':
    # proper()
    wrong()
    pass
