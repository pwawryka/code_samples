""" Example of usage Python meta programming to create equivalent of Scala case classes.
    Module designed only for learning purposes and playing with meta classes.

    TODO:
        * default values
        * type hierarchy for fields (ex. int can be treated as float)
"""
from collections import OrderedDict
from inspect import Parameter, Signature
from unittest import TestCase, main as run_tests


class BaseField:
    _type = None
    _field_name = None

    def __init__(self, const=True):
        self._const = const

    def is_initialized(self, instance):
        return instance.__dict__['_initialized'].get(self._field_name, False)

    def __set__(self, instance, value):
        # Check if property is constant and cannot be set
        if self.is_initialized(instance) and self._const:
            raise AttributeError('Cannot set constant. ({field_name})'.format(field_name=self._field_name))

        # Check proper type of value
        if not isinstance(value, self._type):
            raise TypeError('Wrong value type. ({field_name})'.format(field_name=self._field_name))

        instance.__dict__['_initialized'][self._field_name] = True
        instance.__dict__[self._field_name] = value

    def __get__(self, instance, owner):
        return instance.__dict__.get(self._field_name)

    def __delete__(self, instance):
        raise AttributeError('Cannot delete attribute.')


class Integer(BaseField):
    _type = int


class String(BaseField):
    _type = str


class Float(BaseField):
    _type = float


class CaseClassMeta(type):
    def __prepare__(self, *args, **kwargs):
        return OrderedDict()

    def __new__(mcs, name, bases, cls_dict):
        # Add '_fields' field based on declared class fields
        fields = {key: value for key, value in cls_dict.items() if isinstance(value, BaseField)}
        cls_dict['_fields'] = fields.keys()

        # Add signature based on declared fields
        signature = Signature(Parameter(field, Parameter.POSITIONAL_OR_KEYWORD) for field in fields)
        cls_dict['__signature__'] = signature

        # Add field name based on defined variable name
        for field_name, field in fields.items():
            field._field_name = field_name

        return super().__new__(mcs, name, bases, cls_dict)


class CaseClass(metaclass=CaseClassMeta):
    _fields = []

    def __init__(self, *args, **kwargs):
        self._initialized = {}
        bound_signature = self.__signature__.bind(*args, **kwargs)
        for name, value in bound_signature.arguments.items():
            setattr(self, name, value)

    def __repr__(self):
        fields = ('{name}={value}'.format(name=field, value=getattr(self, field, None)) for field in self._fields)
        fields = ', '.join(fields)
        cls_name = self.__class__.__name__
        return '{cls_name}({fields})'.format(cls_name=cls_name, fields=fields)

    def copy(self, **kwargs):
        init_args = {field: getattr(self, field) for field in self._fields}
        init_args.update(kwargs)
        return self.__class__(**init_args)


class ExampleCC(CaseClass):
    x = Integer()
    y = Float()
    description = String(const=False)


class TestCaseClass(TestCase):
    def test_many_instances(self):
        a = ExampleCC(1, 2., 'test')
        b = ExampleCC(1, 2., 'test')
        self.assertIsNot(a, b)

    def test_wrong_type(self):
        with self.assertRaises(TypeError):
            ExampleCC(1, 2, 3)

    def test_cannot_modify_properties(self):
        obj = ExampleCC(1, 2., 'test')
        with self.assertRaises(AttributeError):
            obj.x = 10

    def test_can_modify_properties_if_not_const(self):
        obj = ExampleCC(1, 2., 'test')
        obj.description = 'hello'
        self.assertEqual('hello', obj.description)

    def test_wrong_type_in_modification(self):
        obj = ExampleCC(1, 2., 'test')
        with self.assertRaises(TypeError):
            obj.description = 3

    def test_copy(self):
        obj = ExampleCC(1, 2., 'test')
        obj_copy = obj.copy(y=3.)
        self.assertEqual(obj_copy.x, 1)
        self.assertEqual(obj_copy.y, 3.)
        self.assertEqual(obj_copy.description, 'test')


if __name__ == '__main__':
    run_tests()
