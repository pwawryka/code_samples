import pickle
import socket
import numpy as np

from django.conf import settings
from image.models import ImageRegion


class BaseCommunicator(object):
    SIZE = 1024
    HOST = settings.CAFFE['COMPARATOR']['HOST']
    PORT = settings.CAFFE['COMPARATOR']['PORT']

    connection = None

    def receive(self):
        data = bytes()
        while True:
            partial_data = self.connection.recv(self.SIZE)
            data += partial_data
            if len(partial_data) != self.SIZE:
                break

        return pickle.loads(data)

    def send(self, data):
        self.connection.send(pickle.dumps(data))


class BaseService(BaseCommunicator):
    def __init__(self):
        super(BaseService, self).__init__()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((self.HOST, self.PORT))
        self.socket.listen(1)

    def request(self):
        self.connection = self.socket.accept()[0]   # just connection
        return self.receive()


class ComparatorService(BaseService):
    regions = None
    descriptor_matrix = None

    def __init__(self):
        super(ComparatorService, self).__init__()
        self._update_data()

    def run(self):
        while True:
            try:
                self.send(self._compare(self.request()))
            except ValueError as e:
                print('COMPARATOR: Value error {}'.format(e))

    def _update_data(self):
        self.regions = ImageRegion.objects.all()
        self.descriptor_matrix = np.array([region.descriptors for region in self.regions]).T
        norm = np.linalg.norm(self.descriptor_matrix, axis=0)
        self.descriptor_matrix /= norm[None, :]

    def _compare(self, vector):
        result = vector.dot(self.descriptor_matrix)
        result = [(region.image_id, result[index], region.id) for index, region in enumerate(self.regions)]
        result.sort(key=lambda e: e[1], reverse=True)
        return result[:10]


class ComparatorRequest(BaseCommunicator):
    def find_similar(self, data):
        try:
            self.connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.connection.connect((self.HOST, self.PORT))
            self.send(data)
            return self.receive()
        except Exception as e:
            print('COMPARATOR REQUEST: Exception {}'.format(e))
            return []
