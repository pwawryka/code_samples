from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics

from .models import SimpleModel
from .serializers import SimpleSerializer


class SimpleListCreateView(APIView):
    def get(self, request):
        examples = SimpleModel.objects.all()
        return Response(SimpleSerializer(examples, many=True).data)

    # def post(self, request):
    #     serializer = SimpleSerializer(data=request.data)
    #     if not serializer.is_valid():
    #         return Response(serializer.errors, status=400)
    #
    #     serializer.save()
    #     return Response(serializer.data)


class SimpleListCreateGenericView(generics.ListAPIView):
    queryset = SimpleModel.objects
    serializer_class = SimpleSerializer


class SimpleGetGenericView(generics.RetrieveAPIView):
    serializer_class = SimpleSerializer
    queryset = SimpleModel.objects
    lookup_field = 'id'
    lookup_url_kwarg = 'simple_id'
