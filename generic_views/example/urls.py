from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.SimpleListCreateView.as_view(), name='simple-list_create'),
    url(r'^generic/$', views.SimpleListCreateGenericView.as_view(), name='generic-simple-list_create'),
    url(r'^generic/(?P<simple_id>\d+)/$', views.SimpleGetGenericView.as_view(), name='generic-simple-get')
]
