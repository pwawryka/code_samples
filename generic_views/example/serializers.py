from rest_framework import serializers

from .models import SimpleModel


class SimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = SimpleModel


