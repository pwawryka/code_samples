from django.db import models


class Stock(models.Model):
    market = models.CharField(max_length=5)
    ticker = models.CharField(max_length=20)
