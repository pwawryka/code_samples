from rest_framework import serializers

from .models import Stock


class StockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stock


class StockExtraSerializer(serializers.ModelSerializer):

    @property
    def data(self):
        data = super(StockExtraSerializer, self).data
        data.update({'extra': 'my_extra_value'})
        return data

    class Meta:
        model = Stock
