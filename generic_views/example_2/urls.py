from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.StockListCreateView.as_view(), name='stock-list_create'),
    url(r'^(?P<market>\w+)/(?P<ticker>\w+)/$', views.StockView.as_view(), name='stock'),
]
