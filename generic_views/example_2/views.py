from django.shortcuts import get_object_or_404
from rest_framework import generics

from .models import Stock
from .serializers import StockSerializer, StockExtraSerializer


class StockListCreateView(generics.ListCreateAPIView):
    queryset = Stock.objects.all()
    serializer_class = StockSerializer


class StockView(generics.RetrieveAPIView):
    serializer_class = StockSerializer

    def get_object(self):
        return get_object_or_404(Stock, market=self.kwargs['market'], ticker=self.kwargs['ticker'])


# class StockView(generics.RetrieveUpdateDestroyAPIView):
#     def get_object(self):
#         return get_object_or_404(Stock, market=self.kwargs['market'], ticker=self.kwargs['ticker'])
#
#     def get_serializer_class(self):
#         return StockExtraSerializer if self.request.method == 'GET' else StockSerializer
#
#     def get_serializer_context(self):
#         context = super(StockView, self).get_serializer_context()
#         context.update({'extra': 'my_extra_value'})
#         return context
